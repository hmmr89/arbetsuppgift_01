package com.work.arbetsuppgift_01.main;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Hammer
 */
public class Arbetsuppgift_01 {

    public static void main(String[] args) {
        ColorString c = new ColorString("{\"name\":\"green\",\"type\":\"color\",\"hex\":\"#00FF00\",\"rgb\":[0,255,0],\"langs\":[\"sv\",\"no\",\"fi\"],\"terms\":[\"gr\u00f6n\",\"gr\u00f8nn\",\"vihre\u00e4\"]}");
        c.printTermRGB("sv");
        c.printTermRGB("no");
        c.printTermRGB("fi");
    }

    public static boolean Luhnalgoritmen(String nr) {
        char[] nrArray = nr.toCharArray();
        if (nrArray.length != 10) {
            return false;
        }

        int value = 0, k = 2;

        for (int x = 0; x < 10; x++) {
            int c = Character.getNumericValue(nrArray[x]);
            int y = c * k;
            k = (k == 2) ? 1 : 2;
            while (y > 0) {
                value += y % 10;
                y = y / 10;
            }
        }
        //Return false if decimal exist.
        return ((double) value / 10 % 1) == 0;
    }

    public static class ColorString {

        private final JsonParserFactory factory;
        private final JSONParser parser;
        private final String color;

        public ColorString(String color) {
            this.factory = JsonParserFactory.getInstance();
            this.parser = factory.newJsonParser();
            this.color = color;
        }

        public void printTermRGB(String lang) {
            Map json = parser.parseJson(color);
            StringBuilder sb = new StringBuilder();

            int langIndex = ((ArrayList<String>) json.get("langs")).indexOf(lang);
            sb.append(((ArrayList) json.get("terms")).get(langIndex));

            String rgbString = json.get("rgb").toString();
            rgbString = rgbString.replace("[", "(").replace("]", ")");

            sb.append(", rgb");
            sb.append(rgbString);
            System.out.println(sb);
        }
    }
}
